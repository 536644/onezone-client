# ProviderSyncProgressAdditionalProperties

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**seq** | **int** | Last sequence number of the corresponding provider that has been seen and processed by *this* provider.  | [optional] 
**timestamp** | [**Timestamp**](Timestamp.md) | Timestamp in seconds (UNIX epoch) when the sequence number has been emitted by the corresponding provider.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


