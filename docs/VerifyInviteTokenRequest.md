# VerifyInviteTokenRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | [**SerializedToken**](SerializedToken.md) |  | 
**peer_ip** | **str** | The IP address of the token bearer. | [optional] 
**consumer_token** | [**SerializedToken**](SerializedToken.md) | Identity token of the consumer that wishes to use the token being verified.  | [optional] 
**expected_invite_type** | **str** | Expected type of the invite token - verification will not succeed if it does not match the actual invite token type.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


