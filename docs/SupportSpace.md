# SupportSpace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**invite_type** | **str** | Invite token type - invitation for a provider to support a space. | [optional] 
**space_id** | **str** | Id of the space to be supported | [optional] 
**parameters** | [**SupportParameters**](SupportParameters.md) | Space support parameters - optional, default to &#x60;dataWrite &#x3D; global&#x60; and &#x60;metadataReplication &#x3D; eager&#x60;.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


