# Time

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **str** | Time caveat - limits the token&#39;s validity in time.  You can learn more about token caveats [here](https://onedata.org/#/home/documentation/doc/using_onedata/tokens[token-caveats].html).  | [optional] 
**valid_until** | [**Timestamp**](Timestamp.md) | Timestamp in seconds (UNIX epoch) when the token expires.  Example: &#x60;&#x60;&#x60;json   {     \&quot;type\&quot;: \&quot;time\&quot;,     \&quot;validUntil\&quot;: 1571147494   } &#x60;&#x60;&#x60;  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


