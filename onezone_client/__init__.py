# coding: utf-8

# flake8: noqa

"""
    Onezone

    # Overview This is the RESTful API definition of Onezone component of Onedata data management system [onedata.org](http://onedata.org).  > This API is defined using [Swagger](http://swagger.io/), the JSON specification can be used to automatically generate client libraries - [swagger.json](../../../swagger/onezone/swagger.json).  This API allows control and configuration of local Onezone service deployment, in particular management of users, groups, spaces, shares, providers, handle services, handles and clusters.  ## Authentication and authorization To be able to use this API, the REST client must authenticate with the Onezone service and posses required authorization, which is determined based on client's privileges and relations in the system.  There are essentially three types of REST clients depending on the authentication:   * **users** - can authenticate using an access token or basic credentials   (only for users originating from Onezone's onepanel). Examples:   ```bash   curl -H \"x-auth-token: $TOKEN\" [...]   curl -H \"authorization: Bearer $TOKEN\" [...]   curl -u \"username:password\" [...]   curl -H \"macaroon: $TOKEN\" [...]   # DEPRECATED   ```   > `$TOKEN` can ba a Onedata access token, obtained via Onezone GUI or API, in the form   `MDAxNWxvY2F00aW9...`. If authority delegation for given IdP is enabled,   it is possible to provide an access token from the IdP, which must be prefixed   properly (depending on the configuration), e.g.: `github/GST5aasdA...`.    * **Oneproviders** - can authenticate using the provider root token,   which was assigned during registration in Onezone. It can be found in   `/etc/op_worker/provider_root_token.txt`. It is used just like a user   access token, for example:   ```bash   curl -H \"x-auth-token: $TOKEN\" [...]   curl -H \"authorization: Bearer $TOKEN\" [...]   curl -H \"macaroon: $TOKEN\" [...]   # DEPRECATED   ```   > Please mind that the provider root token is highly confidential and must   be kept secret (similarly to a private RSA key).    * **anonymous** - there is a small subset of operations that do not require     any authentication and are publicly available (look for information about     public availability in the endpoint descriptions).  The authorization of the client is determined based on existing relations and privileges in the system. In most cases, the rules below can be roughly applied:   * users and providers can access and modify their own data   * users can perform operations in groups, spaces, handle services, handles     and clusters depending on their privileges in subject entity - the required     privileges are listed in the description of each operation   * users can be given special admin privileges (fine-grained) that allow to     access and modify all entities in the system - see certain operations for     details.  Authentication and Authorization errors have the following meaning:   * HTTP 401 UNAUTHORIZED - the client could not be authenticated   * HTTP 403 FORBIDDEN - the client was authenticated, but is not permitted to     perform the action  ## Effective users and effective groups and spaces Onedata supports creation of arbitrary nested group and space membership tree structures. In order to determine if a given user belongs to the group directly or indirectly by belonging to a subgroup of a group, separate API calls are provided for getting information about group users (direct group members) and effective users (indirect group members).  ## API structure The API is divided into several categories, corresponding to entities in Onedata:  **Space management** The space management operations of this API provide means for accessing information about spaces and their management.  **Share management** The share management operations of this API provide means for accessing information about shares and their management.  **Group management** The group management operations allow creation of user groups, assigning their authorization rights, adding and removing users from groups.  **User management** The user management methods allow creation of users, managing their authorization credentials as well as space and group membership.  **Provider management** Provider specific calls allow getting global information about the spaces managed by the provider, and some administrative operations which can be used for monitoring or accounting.  **Handle service management** The handle service management operations of this API provide means for accessing information about handle services and their management.  **Handle API** Onezone provides extensive support for integration with Handle system registration services, including support for DOI and PID identifier assignment services. The API provides methods for adding new Handle services to the system, managing which users can use which registration services and complete API for registering identifiers to users' data sets which are made public.  **Cluster management** Operations for managing Onezone / Oneprovider clusters and their members - users and groups that can access the Onepanel interfaces (REST or GUI) of a cluster.   ## Using the API Onezone API is quite complex and thus it might be difficult to quickly figure out how to perform specific action, however the following guidelines might be useful:   * Operations performed by a regular users on their resources are grouped under     `/user` path (**USER** group in the menu)   * Operations performed by administrators of specific resources (e.g. groups,     spaces, shares) start with specific resource (e.g. `/groups`)   * By default the operations which list resource membership     (e.g. `/spaces/SPACE_ID/groups/`) will list explicit resource membership.     To get list of effective resource membership (i.e. including indirect     membership), special paths are provided     (e.g. `/spaces/SPACE_ID/effective_groups/`)  Furthermore, we have prepared a command-line client environment based on Docker which gives easy access to each of Onedata services via command-line clients, with pre-configured shell with full help on the APIs and autocomplete for operations and attributes.  ``` docker run -it onedata/rest-cli:21.02.3 ```  Below you can find some tutorials which show how to use this API in practice:   * [User oriented tutorial](https://onedata.org/#/home/documentation/doc/using_onedata/using_onedata_from_cli.html)   * [Administrator oriented tutorial](https://onedata.org/#/home/documentation/doc/administering_onedata/administering_onedata_from_cli.html)   ## Examples  **Generate new authentication token** ```bash curl -u user:password -X POST -H 'Content-type: application/json' -d '{}' \\ https://$ONEZONE_HOST/api/v3/onezone/user/client_tokens ```  **Get user details** ```bash curl -H 'X-Auth-Token: $TOKEN' -X GET \\ https://$ONEZONE_HOST/api/v3/onezone/user ```  **Get user details using an access token from github** ```bash curl -H 'X-Auth-Token: github/ijaAVWq3j9234jA9gPoR9agFja89t9UiPf8tiueSdx' -X GET \\ https://$ONEZONE_HOST/api/v3/onezone/user ``` > Note that GitHub IdP must be properly configured for the example to work: > * authority delegation must be enabled > * tokenPrefix must be set to \"github/\" > > You can learn more in > [the documentation](https://onedata.org/#/home/documentation/doc/administering_onedata/openid_saml_configuration/openid_saml_configuration_19_02[authority-delegation].html).   # noqa: E501

    OpenAPI spec version: 21.02.3
    Contact: info@onedata.org
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

# import apis into sdk package
from onezone_client.cluster_api import ClusterApi
from onezone_client.group_api import GroupApi
from onezone_client.handle_api import HandleApi
from onezone_client.handle_service_api import HandleServiceApi
from onezone_client.harvester_api import HarvesterApi
from onezone_client.provider_api import ProviderApi
from onezone_client.share_api import ShareApi
from onezone_client.space_api import SpaceApi
from onezone_client.token_api import TokenApi
from onezone_client.user_api import UserApi
from onezone_client.zone_api import ZoneApi

# import ApiClient
from onezone_client.api_client import ApiClient
from onezone_client.configuration import Configuration
# import models into sdk package
from onezone_client.onezone_client.admin_privileges import AdminPrivileges
from onezone_client.onezone_client.admin_privileges_update import AdminPrivilegesUpdate
from onezone_client.onezone_client.api import Api
from onezone_client.onezone_client.asn import Asn
from onezone_client.onezone_client.caveat import Caveat
from onezone_client.onezone_client.client_token import ClientToken
from onezone_client.onezone_client.client_tokens import ClientTokens
from onezone_client.onezone_client.cluster import Cluster
from onezone_client.onezone_client.cluster_invite_token import ClusterInviteToken
from onezone_client.onezone_client.cluster_manager_privileges import ClusterManagerPrivileges
from onezone_client.onezone_client.cluster_member_privileges import ClusterMemberPrivileges
from onezone_client.onezone_client.cluster_privileges import ClusterPrivileges
from onezone_client.onezone_client.cluster_privileges_update import ClusterPrivilegesUpdate
from onezone_client.onezone_client.cluster_update_request import ClusterUpdateRequest
from onezone_client.onezone_client.clusters import Clusters
from onezone_client.onezone_client.configuration import Configuration
from onezone_client.onezone_client.configuration_supported_id_ps import ConfigurationSupportedIdPs
from onezone_client.onezone_client.consumer import Consumer
from onezone_client.onezone_client.doi_service_properties import DOIServiceProperties
from onezone_client.onezone_client.doi_service_properties_update import DOIServicePropertiesUpdate
from onezone_client.onezone_client.data import Data
from onezone_client.onezone_client.data1 import Data1
from onezone_client.onezone_client.data10 import Data10
from onezone_client.onezone_client.data11 import Data11
from onezone_client.onezone_client.data12 import Data12
from onezone_client.onezone_client.data13 import Data13
from onezone_client.onezone_client.data14 import Data14
from onezone_client.onezone_client.data15 import Data15
from onezone_client.onezone_client.data16 import Data16
from onezone_client.onezone_client.data17 import Data17
from onezone_client.onezone_client.data2 import Data2
from onezone_client.onezone_client.data3 import Data3
from onezone_client.onezone_client.data4 import Data4
from onezone_client.onezone_client.data5 import Data5
from onezone_client.onezone_client.data6 import Data6
from onezone_client.onezone_client.data7 import Data7
from onezone_client.onezone_client.data8 import Data8
from onezone_client.onezone_client.data9 import Data9
from onezone_client.onezone_client.data_objectid import DataObjectid
from onezone_client.onezone_client.data_path import DataPath
from onezone_client.onezone_client.data_readonly import DataReadonly
from onezone_client.onezone_client.error import Error
from onezone_client.onezone_client.error_error import ErrorError
from onezone_client.onezone_client.examined_token import ExaminedToken
from onezone_client.onezone_client.geo_country import GeoCountry
from onezone_client.onezone_client.geo_region import GeoRegion
from onezone_client.onezone_client.group import Group
from onezone_client.onezone_client.group_create_request import GroupCreateRequest
from onezone_client.onezone_client.group_invite_token import GroupInviteToken
from onezone_client.onezone_client.group_join_cluster import GroupJoinCluster
from onezone_client.onezone_client.group_join_group import GroupJoinGroup
from onezone_client.onezone_client.group_join_harvester import GroupJoinHarvester
from onezone_client.onezone_client.group_join_space import GroupJoinSpace
from onezone_client.onezone_client.group_manager_privileges import GroupManagerPrivileges
from onezone_client.onezone_client.group_member_privileges import GroupMemberPrivileges
from onezone_client.onezone_client.group_privileges import GroupPrivileges
from onezone_client.onezone_client.group_privileges_update import GroupPrivilegesUpdate
from onezone_client.onezone_client.group_update_request import GroupUpdateRequest
from onezone_client.onezone_client.groups import Groups
from onezone_client.onezone_client.handle import Handle
from onezone_client.onezone_client.handle_member_privileges import HandleMemberPrivileges
from onezone_client.onezone_client.handle_privileges import HandlePrivileges
from onezone_client.onezone_client.handle_privileges_update import HandlePrivilegesUpdate
from onezone_client.onezone_client.handle_registration_request import HandleRegistrationRequest
from onezone_client.onezone_client.handle_service import HandleService
from onezone_client.onezone_client.handle_service_create_request import HandleServiceCreateRequest
from onezone_client.onezone_client.handle_service_member_privileges import HandleServiceMemberPrivileges
from onezone_client.onezone_client.handle_service_privileges import HandleServicePrivileges
from onezone_client.onezone_client.handle_service_privileges_update import HandleServicePrivilegesUpdate
from onezone_client.onezone_client.handle_service_properties import HandleServiceProperties
from onezone_client.onezone_client.handle_service_properties_update import HandleServicePropertiesUpdate
from onezone_client.onezone_client.handle_service_update import HandleServiceUpdate
from onezone_client.onezone_client.handle_services import HandleServices
from onezone_client.onezone_client.handles import Handles
from onezone_client.onezone_client.harvester import Harvester
from onezone_client.onezone_client.harvester_create_request import HarvesterCreateRequest
from onezone_client.onezone_client.harvester_gui_plugin_config import HarvesterGuiPluginConfig
from onezone_client.onezone_client.harvester_index import HarvesterIndex
from onezone_client.onezone_client.harvester_index_create_request import HarvesterIndexCreateRequest
from onezone_client.onezone_client.harvester_index_stats_details import HarvesterIndexStatsDetails
from onezone_client.onezone_client.harvester_index_stats_details_space_id import HarvesterIndexStatsDetailsSpaceId
from onezone_client.onezone_client.harvester_index_stats_details_space_id_provider_id import HarvesterIndexStatsDetailsSpaceIdProviderId
from onezone_client.onezone_client.harvester_indices import HarvesterIndices
from onezone_client.onezone_client.harvester_invite_token import HarvesterInviteToken
from onezone_client.onezone_client.harvester_join_space import HarvesterJoinSpace
from onezone_client.onezone_client.harvester_manager_privileges import HarvesterManagerPrivileges
from onezone_client.onezone_client.harvester_member_privileges import HarvesterMemberPrivileges
from onezone_client.onezone_client.harvester_privileges import HarvesterPrivileges
from onezone_client.onezone_client.harvester_privileges_update import HarvesterPrivilegesUpdate
from onezone_client.onezone_client.harvester_query import HarvesterQuery
from onezone_client.onezone_client.harvester_query_response import HarvesterQueryResponse
from onezone_client.onezone_client.harvester_update_request import HarvesterUpdateRequest
from onezone_client.onezone_client.harvesters import Harvesters
from onezone_client.onezone_client.id_p_access_token import IdPAccessToken
from onezone_client.onezone_client.inline_response200 import InlineResponse200
from onezone_client.onezone_client.inline_response2001 import InlineResponse2001
from onezone_client.onezone_client.inline_response20010 import InlineResponse20010
from onezone_client.onezone_client.inline_response20011 import InlineResponse20011
from onezone_client.onezone_client.inline_response20012 import InlineResponse20012
from onezone_client.onezone_client.inline_response20013 import InlineResponse20013
from onezone_client.onezone_client.inline_response20014 import InlineResponse20014
from onezone_client.onezone_client.inline_response20015 import InlineResponse20015
from onezone_client.onezone_client.inline_response20016 import InlineResponse20016
from onezone_client.onezone_client.inline_response20017 import InlineResponse20017
from onezone_client.onezone_client.inline_response20018 import InlineResponse20018
from onezone_client.onezone_client.inline_response20019 import InlineResponse20019
from onezone_client.onezone_client.inline_response2002 import InlineResponse2002
from onezone_client.onezone_client.inline_response20020 import InlineResponse20020
from onezone_client.onezone_client.inline_response2003 import InlineResponse2003
from onezone_client.onezone_client.inline_response2004 import InlineResponse2004
from onezone_client.onezone_client.inline_response2005 import InlineResponse2005
from onezone_client.onezone_client.inline_response2006 import InlineResponse2006
from onezone_client.onezone_client.inline_response2007 import InlineResponse2007
from onezone_client.onezone_client.inline_response2007_spaces import InlineResponse2007Spaces
from onezone_client.onezone_client.inline_response2008 import InlineResponse2008
from onezone_client.onezone_client.inline_response2009 import InlineResponse2009
from onezone_client.onezone_client.interface import Interface
from onezone_client.onezone_client.invite_token import InviteToken
from onezone_client.onezone_client.invite_token_property_privileges import InviteTokenPropertyPrivileges
from onezone_client.onezone_client.invite_token_property_usage_limit import InviteTokenPropertyUsageLimit
from onezone_client.onezone_client.ip import Ip
from onezone_client.onezone_client.linked_account import LinkedAccount
from onezone_client.onezone_client.listing_options import ListingOptions
from onezone_client.onezone_client.membership_intermediaries import MembershipIntermediaries
from onezone_client.onezone_client.membership_intermediaries_intermediaries import MembershipIntermediariesIntermediaries
from onezone_client.onezone_client.name import Name
from onezone_client.onezone_client.named_token import NamedToken
from onezone_client.onezone_client.named_token_create_request import NamedTokenCreateRequest
from onezone_client.onezone_client.named_token_create_response import NamedTokenCreateResponse
from onezone_client.onezone_client.named_token_status import NamedTokenStatus
from onezone_client.onezone_client.named_token_update_request import NamedTokenUpdateRequest
from onezone_client.onezone_client.pid_service_properties import PIDServiceProperties
from onezone_client.onezone_client.pid_service_properties_update import PIDServicePropertiesUpdate
from onezone_client.onezone_client.privileges import Privileges
from onezone_client.onezone_client.privileges1 import Privileges1
from onezone_client.onezone_client.provider_details import ProviderDetails
from onezone_client.onezone_client.provider_domain_config import ProviderDomainConfig
from onezone_client.onezone_client.provider_registration_request import ProviderRegistrationRequest
from onezone_client.onezone_client.provider_registration_response import ProviderRegistrationResponse
from onezone_client.onezone_client.provider_registration_token import ProviderRegistrationToken
from onezone_client.onezone_client.provider_sync_progress import ProviderSyncProgress
from onezone_client.onezone_client.provider_sync_progress_additional_properties import ProviderSyncProgressAdditionalProperties
from onezone_client.onezone_client.provider_update_request import ProviderUpdateRequest
from onezone_client.onezone_client.providers import Providers
from onezone_client.onezone_client.register_oneprovider import RegisterOneprovider
from onezone_client.onezone_client.request import Request
from onezone_client.onezone_client.request1 import Request1
from onezone_client.onezone_client.serialized_token import SerializedToken
from onezone_client.onezone_client.service import Service
from onezone_client.onezone_client.share import Share
from onezone_client.onezone_client.shares import Shares
from onezone_client.onezone_client.space import Space
from onezone_client.onezone_client.space_advertised_in_marketplace import SpaceAdvertisedInMarketplace
from onezone_client.onezone_client.space_alias import SpaceAlias
from onezone_client.onezone_client.space_create_request import SpaceCreateRequest
from onezone_client.onezone_client.space_description import SpaceDescription
from onezone_client.onezone_client.space_invite_token import SpaceInviteToken
from onezone_client.onezone_client.space_join_harvester import SpaceJoinHarvester
from onezone_client.onezone_client.space_manager_privileges import SpaceManagerPrivileges
from onezone_client.onezone_client.space_marketplace_contact_email import SpaceMarketplaceContactEmail
from onezone_client.onezone_client.space_marketplace_data import SpaceMarketplaceData
from onezone_client.onezone_client.space_member_privileges import SpaceMemberPrivileges
from onezone_client.onezone_client.space_membership_request import SpaceMembershipRequest
from onezone_client.onezone_client.space_membership_requester_info import SpaceMembershipRequesterInfo
from onezone_client.onezone_client.space_membership_requests import SpaceMembershipRequests
from onezone_client.onezone_client.space_name import SpaceName
from onezone_client.onezone_client.space_organization_name import SpaceOrganizationName
from onezone_client.onezone_client.space_privileges import SpacePrivileges
from onezone_client.onezone_client.space_privileges_update import SpacePrivilegesUpdate
from onezone_client.onezone_client.space_stats import SpaceStats
from onezone_client.onezone_client.space_support_token import SpaceSupportToken
from onezone_client.onezone_client.space_tags import SpaceTags
from onezone_client.onezone_client.spaces import Spaces
from onezone_client.onezone_client.subject import Subject
from onezone_client.onezone_client.support_parameters import SupportParameters
from onezone_client.onezone_client.support_space import SupportSpace
from onezone_client.onezone_client.support_stage_details import SupportStageDetails
from onezone_client.onezone_client.temporary_token_create_request import TemporaryTokenCreateRequest
from onezone_client.onezone_client.temporary_token_generation import TemporaryTokenGeneration
from onezone_client.onezone_client.time import Time
from onezone_client.onezone_client.timestamp import Timestamp
from onezone_client.onezone_client.token_property_caveats import TokenPropertyCaveats
from onezone_client.onezone_client.token_property_custom_metadata import TokenPropertyCustomMetadata
from onezone_client.onezone_client.token_property_id import TokenPropertyId
from onezone_client.onezone_client.token_property_metadata import TokenPropertyMetadata
from onezone_client.onezone_client.token_property_name import TokenPropertyName
from onezone_client.onezone_client.token_property_onezone_domain import TokenPropertyOnezoneDomain
from onezone_client.onezone_client.token_property_persistence import TokenPropertyPersistence
from onezone_client.onezone_client.token_property_revoked import TokenPropertyRevoked
from onezone_client.onezone_client.token_property_subject import TokenPropertySubject
from onezone_client.onezone_client.token_property_token_type import TokenPropertyTokenType
from onezone_client.onezone_client.tokens import Tokens
from onezone_client.onezone_client.user import User
from onezone_client.onezone_client.user_access_block_update import UserAccessBlockUpdate
from onezone_client.onezone_client.user_basic_auth_settings_update import UserBasicAuthSettingsUpdate
from onezone_client.onezone_client.user_create_request import UserCreateRequest
from onezone_client.onezone_client.user_join_cluster import UserJoinCluster
from onezone_client.onezone_client.user_join_group import UserJoinGroup
from onezone_client.onezone_client.user_join_harvester import UserJoinHarvester
from onezone_client.onezone_client.user_join_space import UserJoinSpace
from onezone_client.onezone_client.user_password_update import UserPasswordUpdate
from onezone_client.onezone_client.user_protected_info import UserProtectedInfo
from onezone_client.onezone_client.user_update_request import UserUpdateRequest
from onezone_client.onezone_client.users import Users
from onezone_client.onezone_client.verify_access_token_request import VerifyAccessTokenRequest
from onezone_client.onezone_client.verify_identity_token_request import VerifyIdentityTokenRequest
from onezone_client.onezone_client.verify_invite_token_request import VerifyInviteTokenRequest
from onezone_client.onezone_client.verify_token_response import VerifyTokenResponse
from onezone_client.onezone_client.version_info import VersionInfo
from onezone_client.onezone_client.viewer_privileges import ViewerPrivileges
